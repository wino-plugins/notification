const ejs_template = require('./template')

async function publishNotification(payload) {
   try{
      await Registry.executeServiceFunction('notification:sendNotification', payload)
   }catch(err){
      if([3300, 3301, 3302, 3303, '01-04', '001-13'].includes(err?.code || err?.metadata?.code)){
         let queued_notifications = await RedisClient.Get("queued_notifications", true)

         // Check if there are previous queues and initialize to prevent errors, push to queue and save it to redis
         if(!queued_notifications || !Array.isArray(queued_notifications)){queued_notifications = []}
         queued_notifications.push(payload)
         await RedisClient.Set("queued_notifications", queued_notifications, true);
      }else{
         throw err
      }
   }
}

async function sendNotification({channel,to,message,data,attachments,headers}){
   try {
      console.log("Notification Req headers",headers);
      await publishNotification({
         to,
         attachments,
         channel,
         service: configs.registry.appName,
         data,
         message,
         institution_code: headers?.institution || "puma-001" 
      })
   } catch (err) {
      throw err
   }
}

async function sendNotificationTemplate({channel,to,data,template_data,template,attachments,headers}){
   try {
       console.log("Notification Req headers",headers);
      if(channel === 'EMAIL'){
         
         await publishNotification({
            to,
            attachments,
            channel,
            service: configs.registry.appName,
            data,
            message_type: 'html',
            message: await ejs_template.getMailContent(template,template_data),
            institution_code: headers?.institution || "puma-001" 
         })
      }else if(channel === 'SMS'){
         await publishNotification({
            to,
            channel,
            data,
            service: configs.registry.appName,
            message: await ejs_template.getMailContent(template,template_data),
            institution_code: headers?.institution || "puma-001" 
         })
      }

      return true
   } catch (err) {
      throw err
   }
}

module.exports = {
   sendNotification,
   sendNotificationTemplate
}
